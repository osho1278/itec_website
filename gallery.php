
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>ITEC Kanpur</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

 <link href="css/spinningicons.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
<link rel="stylesheet" href="css/bootstrap-gallery.css">
<link rel="stylesheet" href="css/blueimp-gallery.css">
<link rel="stylesheet" href="css/bootstrap-image-gallery.min.css">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="css/carousel.css" rel="stylesheet">

<style>

#taketest {

    position: fixed;
    bottom: 0;
    width: 30%;
    color:purple;
}

</style>

  </head>
<!-- NAVBAR
================================================== -->
  <body>
    <div class="navbar-wrapper">
      <div class="container">

        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="index.php" style="color:orange">ITEC- Information Technology Education Center </a>
            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav navbar-right">
                <li ><a href="index.php">Home</a></li>
                <li><a href="about.php">About</a></li>
                <li><a href="courses.php">Courses</a></li>
                <li class="active"><a href="gallery.php">Gallery</a></li>
                <li><a href="contact.php">Contact</a></li>
                
              </ul>
            </div>
          </div>
        </div>

<br><br>
                      <img src="images/Graphic1.jpg" width="100%" height="180px">

      </div>
    </div>

    <!-- Carousel
    ================================================== -->
   



    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->
     <br><br><br>
     <br><br><br>
     <br><br><br>

    <div class="container marketing">

      <!-- Three columns of text below the carousel -->
     <br><br><br>
<h1 style="color:brown">Gallery</h1>

      <!-- START THE FEATURETTES -->

     
    
      <!-- /END THE FEATURETTES -->
 <div id="blueimp-gallery" class="blueimp-gallery">
    <!-- The container for the modal slides -->
    <div class="slides"></div>
    <!-- Controls for the borderless lightbox -->
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
    <!-- The modal dialog, which will be used to wrap the lightbox content -->
    <div class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body next"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left prev">
                        <i class="glyphicon glyphicon-chevron-left"></i>
                        Previous
                    </button>
                    <button type="button" class="btn btn-primary next">
                        Next
                        <i class="glyphicon glyphicon-chevron-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="links">
    <a href="galleryimages/classroom 1.jpg" title="Classroom 1" data-gallery>
        <img src="galleryimages/classroom 1.jpg" alt="Banana" class="img-thumbnail" height="140" width="140">
    </a>
<a href="galleryimages/classroom 2.jpg" title="Classroom 2" data-gallery>
        <img src="galleryimages/classroom 2.jpg" alt="Banana" class="img-thumbnail" height="140" width="140">
    </a>
    <a href="galleryimages/classroom 3.jpg" title="Classroom 3" data-gallery>
        <img src="galleryimages/classroom 3.jpg" alt="Banana" class="img-thumbnail" height="140" width="140">
    </a>
    <a href="galleryimages/LAB 1.jpg" title="LAB 1" data-gallery>
        <img src="galleryimages/LAB 1.jpg" alt="Banana" class="img-thumbnail" height="140" width="140">
    </a>
    <a href="galleryimages/LAB 2.jpg" title="LAB 2" data-gallery>
        <img src="galleryimages/LAB 2.jpg" alt="Banana" class="img-thumbnail" height="140" width="140">
    </a>
    <a href="galleryimages/LAB 3.jpg" title="LAB #3" data-gallery>
        <img src="galleryimages/LAB 3.jpg" alt="Banana" class="img-thumbnail" height="140" width="140">
    </a>
        <a href="galleryimages/lobby.jpg" title="lobby" data-gallery>
        <img src="galleryimages/lobby.jpg" alt="Banana" class="img-thumbnail" height="140" width="140">
    </a>
    <a href="galleryimages/ITEC BUILDING.jpg" title="ITEC Building" data-gallery>
        <img src="galleryimages/ITEC BUILDING.jpg" alt="Banana" class="img-thumbnail" height="140" width="140">
    </a>
    <a href="galleryimages/ITEC OFFICE.jpg" title="OFFICE" data-gallery>
        <img src="galleryimages/ITEC OFFICE.jpg" alt="Banana" class="img-thumbnail" height="140" width="140">
    </a>
    

</div>


      <hr class="featurette-divider">

      <!-- FOOTER -->
          <footer>
        <center> <a href="index.php"><span class="glyphicon glyphicon-home"> Home</span></a> |
         <a href="about.php"><span class="glyphicon glyphicon-thumbs-up"> About </span></a> |
          <a href="courses.php"><span class="glyphicon glyphicon-folder-open"> Courses </span></a> |
<a href="gallery.php"><span class="glyphicon glyphicon-camera"> Gallery</span> |
<a href="contact.php"><span class="glyphicon glyphicon-phone-alt"> Contact</span></a>
         </center>
<!--
         <div class="spinning_icons pull-center">  
        Follow us on : <br>
        <a href="https://twitter.com/delphictechno" class="twitter" title="twitter">Twitter</a>
        <a href="https://www.facebook.com/delphictech" class="facebook" title="facebook">Facebook</a>
        <a href="http://in.linkedin.com/pub/delphic-tech/91/a7b/49/" class="linkedin" title="linkedin">Linkedin</a>
        <a href="hhttp://www.youtube.com/channel/UCDFzBhd9t1L5kIsqmuo6kcw/videos" class="youtube" title="youtube">YouTube</a>
          </div>
  </center> -->
   <p>&copy; 2014 ITEC - Kanpur , Inc.</p>
     
      </footer>

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/google.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>
<script src="js/gallery-img.js"></script>
<script src="js/bootstrap-image-gallery.min.js"></script>

  </body>
</html>
