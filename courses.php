
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>ITEC Kanpur</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

 <link href="css/spinningicons.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
<link rel="stylesheet" href="css/bootstrap-gallery.css">
<link rel="stylesheet" href="css/blueimp-gallery.css">
<link rel="stylesheet" href="css/bootstrap-image-gallery.min.css">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="css/carousel.css" rel="stylesheet">

<style>
.side-button
{
position:fixed;
margin-left: 80%;


}
#book
{
margin-left: 80%;
margin-top: 8%;
position :fixed;

}
#menu{

  position: relative;
}
#taketest {

    position: fixed;
    bottom: 0;
    width: 30%;
    color:purple;
}
.fixed
{
  position:fixed;
}
</style>
<script>
function call_java(a)
{
document.getElementById(a).innerHTML=document.getElementById(a).innerHTML+'<div class="col-sm-4" style="position :fixed;"><a href="#">Computer Application</a></div>';
}
</script>

  </head>
<!-- NAVBAR
================================================== -->
  <body>
    <div class="navbar-wrapper">
      <div class="container">

        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="index.php" style="color:orange">ITEC- Information Technology Education Center </a>
            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav navbar-right">
                <li><a href="index.php">Home</a></li>
                <li><a href="about.php">About</a></li>
                <li class="active"><a href="courses.php">Courses</a></li>
                <li ><a href="gallery.php">Gallery</a></li>
                <li ><a href="contact.php">Contact</a></li>
                
              </ul>
            </div>
          </div>
        </div>
<br><br>
                      <img src="images/Graphic1.jpg" width="100%" height="180px">

      </div>
    </div>

    <!-- Carousel
    ================================================== -->
   



    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->
    <div class="container marketing">
<br>
<br>
<br><bR>
<br>
<br>
<br><bR>
    <!-- Three columns of text below the carousel -->
     
<h1 style="color:brown">Courses to choose from : </h1>
      <!-- START THE FEATURETTES -->

<div class="row" id="courses">
            <div class="col-6 col-sm-6 col-lg-4">
              <h2 style="background:orange; color:black">Computer Application</h2>
              <p>Application software is a set of one or more programs designed to carry out operations for a specific application. Application software cannot run on itself but is dependent on system software to execute. </p>
              <br><br>
              <p><a class="btn btn-success" href="#1" role="button" onclick="call_java('1')">View details &raquo;</a></p>
            </div><!--/span-->
            <div class="col-6 col-sm-6 col-lg-4">
              <h2 style="background:orange; color:black">Accounting / Taxation</h2>
              <p>Accounting, or accountancy, is the measurement, processing and communication of financial information about economic entities. </p>
              <br><br><br><p><a class="btn btn-success" href="#2" role="button">View details &raquo;</a></p>
            </div><!--/span-->
            <div class="col-6 col-sm-6 col-lg-4">
              <h2 style="background:orange; color:black">Web Programming</h2>
              <p>Web development is a broad term for the work involved in developing a web site for the Internet or an intranet. Web development can range from developing the simplest static single page of plain text to the most complex web-based internet applications, electronic businesses, and social network services </p>
              <p><a class="btn btn-success" href="#3" role="button">View details &raquo;</a></p>
            </div><!--/span-->
            <div class="col-6 col-sm-6 col-lg-4">
              <h2 style="background:orange; color:black">Hardware / Networking</h2>
              <p>Networking hardware may also be known as network equipment or computer networking devices. Units which are the last receiver or generate data are called hosts or data terminal equipment. </p>
            <br><br><br><p><a class="btn btn-success" href="#4" role="button">View details &raquo;</a></p>
             </div><!--/span-->
            <div class="col-6 col-sm-6 col-lg-4">
              <h2 style="background:orange; color:black">Software Programming</h2>
              <p>Computer programming (often shortened to programming) is a process that leads from an original formulation of a computing problem to executable programs. It involves activities such as analysis, understanding, and generically solving such problems resulting in an algorithm, verification of requirements of the algorithm. </p>
              <p><a class="btn btn-success" href="#5" role="button">View details &raquo;</a></p>
            </div><!--/span-->
            <div class="col-6 col-sm-6 col-lg-4">
              <h2 style="background:orange; color:black">Graphics Designing</h2>
              <p>Graphic design is the art of communication, stylizing, and problem-solving through the use of type, space and image. The field is considered a subset of visual communication and communication design, but sometimes the term "graphic design" is used interchangeably with these due to overlapping skills involved. </p>
              <p><a class="btn btn-success" href="#6" role="button">View details &raquo;</a></p>
            </div><!--/span-->
          </div><!--/row-->
       
<div class="col-sm-12">
<div  id="1">
<h1>Computer Applications </h1>
<ol class="breadcrumb">You are here :
  <li><a href="#courses">Courses</a></li>
  <li><a href="#1">Computer Applications</a></li>
  <li class="active">PGDCA</li>
</ol>

<h3><code>1. PGDCA (Post Graduate Diploma in Computer Applications)</code></h3>
<table class="table">
<tr><td>Objective </td><td> PGDCA program is designed with the objective knowledge and skills in the application of computer. The programme gives fundamental knowledge of computers, computational techniques, file and data structure and the core of programming. Learners will also be trained in the latest trends of Application Development, Programming Languages and Database Management. PGDCA equip learners with skills required for high-end applications in Information Technology.</td>
</tr>

<tr><td>Duration:</td><td> 1 Year</td></tr>
<tr><td>Eligibility</td><td> Graduate in any discipline</td></tr>
<tr><td>Curriculam</td><td>
<ul><li><b>Semester I: </b> <ul><li>Introduction to Computer & English/Hindi typing</li> <li>Fundamentals of Computer & Information Technology Management,</li><li>Office Automation</li> <li>Operating System (DOS / WINDOWS Or XP)</li><li> C</li> <li>C++</li><li>Practical Lab</li></ul>
</li>
<li><b>Semester II: </b><ul><li>RDBMS & Foxpro</li><li> Internet & Web Designing</li> <li>System Analysis & Design & OOPS</li><li> .Net/VB (optional)</li><li> Practical Lab</li><li> Project</li>
</ul>
</li>
</ul>
<tr><td>Exit Profile:</td><td>After Completion of the DEO Course from ITRC, You can start working as Office Assistant, Data Entry Operator, Back office Assistant, Computer Operator, Computer Teacher, MIS Executive.
</td>
</tr>
</td>
</tr>
</table>
<ol class="breadcrumb">You are here :
  <li><a href="#courses">Courses</a></li>
  <li><a href="#1">Computer Applications</a></li>
  <li class="active">DCA</li>
</ol>
<h3><code>2: DCA (Diploma in Computer Applications) </code></h3>
<table class="table">
<tr><td>Objective:</td><td>This one year program designed to impart knowledge in the field of computer application. Today Companies require experts to handle their I.T. Technology and communication system. DCA program is designed to impart knowledge to computer professionals about different subjects like Database, Programming, Operating System, etc. </td></tr>
<tr><td>Duration:</td><td> 1 Year</td>
<tr><td>Eligibility</td><td> High School or Equivalent</td>

<tr><td>Curriculam</td>
<td><ul><li>
Semester I:<ul><li>FOC</li><li> DOS</li><li> MS Office</li><li> Programming in C</li><li> Internet</li></ul>
</li>
<li>
Semester: II<ul><li>
Accounting Software (Tally)</li><li>PageMaker</li> <li>Photoshop</li> <li>CorelDraw</li><li> Project</li>
 </ul>
</li>
</ul>
<tr><td>Exit Profile: </td><td>After Completion of the DCA Course from ITRC, You can start working as Office Assistant, Data Entry Operator, Back office Assistant, Computer Operator, Office Manager, MIS Executivee.</td>
</tr>
</td>
</tr>
</tr>
</tr>
</table>
<ol class="breadcrumb"> You are here : 
  <li><a href="#courses">Courses</a></li>
  <li><a href="#1">Computer Applications</a></li>
  <li class="active">BCC</li>
</ol>
<h3><code>3: (BCC) Basic Computer Course </code></h3>
<table class="table">
<tr>
<td>
Objective: </td><td>The main objectives of this course to provides basic training of computer and its most common software use in office work. This course is most beneficial for beginners.</td></tr>
<tr><td>
Duration:</td><td> 3 Months</td></tr>

<tr><td>Eligibility:</td><td> No Formal Qualification</td></tr>

<tr>
<td>
Curriculam:</td><td><ul><li> FOC</li> <li> DOS</li><li> Windows</li> <li>MS Word</li><li> MS Excel</li><li> MS PowerPoint</li><li> Internet</li></ul></td></tr>
<tr><td>Exit Profile:</td><td> After Completion of the NCLM Course from ITRC, You can start working as Office Assistant, Data Entry Operator, Back office Assistant, Computer Operator.</td>
</tr>
</table>




</div>    


<!-- Accounting and taxation details go here -->

<div id="2">
<h1>Accounting / Taxation</h1>
<ol class="breadcrumb">You are here :
  <li><a href="#courses">Courses</a></li>
  <li><a href="#2">Accounting & Taxation</a></li>
  <li class="active">CCA</li>
</ol>
<h3><code>1: CCA(Certificate in Computerized accounting)</code></h3>
<table class="table">
<tr>
<td>
Objective:</td><td> The objective of the Computerized Accounting program is to give the student "hands on" training in the key areas of financial and management accounting, as well as in the implementation and operation of a computerized accounting system. </td></tr>

<tr><td>Duration:</td><td> 3 Month</td></tr>

<tr><td>Eligibility:</td><td> Higher Secondary or Equivalent</td></tr>

<tr><td>Curriculam:</td><td><ul><li>Accounting Concepts and Conventions</li><li> Journal Entries Voucher Preparation (Manual in Tally 4.5/5.4/6.3/7.2)</li><li> Bank Reconciliation (Manual in Tally)</li><li> Final Accounts</li><li> Tally 4.5/5.4/6.3/7.2</li><li>Internet</li></ul></td></tr>

<tr><td>Exit Profile:</td><td> After Completion of this Course from ITRC, You can start working as Account Executive, MIS Executive, Finance Executive.</td></tr></table>


<ol class="breadcrumb">You are here :
  <li><a href="#courses">Courses</a></li>
  <li><a href="#2">Accounting & Taxation</a></li>
  <li class="active">DFA</li>
</ol>

<h3><code>2: DFA (Diploma in financial Accounting)</code></h3>
<table class="table">
<tr><Td>Objective:</Td><td> The objective of the program is to familiarize the students with basics of accounting, finance, banking transactions and taxation. Students specifically shall be trained on integration of these functions with computers including learning of Tally software, MS-office</td></tr>
<tr><td>Duration:</td><td> 6 Months</td></tr>
<tr><td>Eligibility:</td><td> High School or Equivalent</td></tr>

<tr><td>Curriculam:</td><td><ul><li>FOC</li><li> DOS</li><li> Windows</li><li> MS Word</li><li> Ms Excel</li><li> Accounting Concepts and Conventions</li><li> Journal Entries and Voucher Preparation(Manual and in Tally 4.5/5.4/6.3/7.2)</li><li> Bank Reconciliation(Manual and in Tally 5.4)</li><li> Final Accounts</li><li> Tally 4.5/5.4/7.2/6.3/7.2</li><li> Internet</li></ul></td></tr>

<tr><td>
Exit Profile:</td><td> After Completion of the DFA Course from ITRC, You can start working as Account Experts, Account Executive, Excise Executive, Taxation Executive, MIS Executive, Finance Executive.</td></tr></table>



<ol class="breadcrumb">You are here :
  <li><a href="#courses">Courses</a></li>
  <li><a href="#2">Accounting & Taxation</a></li>
  <li class="active">ADFA</li>
</ol>





<h3><code>3: ADFA(Advance diploma in financial Accounting)</code></h3>
<table class="table">
<tr><td>Objective:</td><td>  The objective of the Computerized Accounting program is to give the student "hands on" training in the key areas of financial and management accounting, as well as in the implementation and operation of a computerized accounting system. The practical, rather than the theoretical, is stressed. The successful graduate will have the necessary skills to keep a set of books, both manually and on a computerized system. </td></tr>

<tr><td>Duration:</td><td> 1 Year</td></tr>

<tr><td>Eligibility:</td><td> Higher Secondary or equivalent</td></tr>
<tr><td>Curriculam</td><td>

<ul><li>Semester I:</li>
<ul><li>
Fundamental of Computers</li><li> DOS</li><li> Windows</li><li> Accounting Concepts and Conventions</li><li> Double Entry System of Book Keeping</li><li> Rules to Journalize the transaction(Practical on Tally 5.4/7.2 and Manually)</li><li> Rectification of Errors(Practical on Tally 4.5/5.4/6.3 and manually)</li><li> Basic of Tally 5.4/7.2 –<ul><li> (a) Creation of Company, </li><li>(b) creation of Groups and Ledger</li><li> (c) Inventory Accounting</li><li> (d) Short cut keys</li><li> (e) Bank Reconciliation on Tally 5.4 manually</li></ul></li><li> MS Office(MS Word, MS Excel)</li></ul>

<li>
Semester II:
</li>
<ul>
<li>Adjustment entries</li><li> Introduction to trial balance</li><li> Ledger</li><li> Journal</li> <li>Cash Book</li> <li>Balance Sheet , Profit and Loss A/c</li><li> Final A/c</li><li> Mgf A/c</li> <li>Trading A/c</li><li> Income/Expd. A/c</li><li> Consignment Accounting</li><li> Internet and Basic of E-Commerce</li> <li>Inventory Maint.& other Advanced usages of Tally4.5/5.4/6.3/7.2/8.1/9(optional)</li><li> Basic of Taxation (C.T., I.T., E.T., P.T.)</li><li> Project.</li></ul>
</ul>
</td></tr>


<tr><td>Exit Profile:</td><td> After Completion of the ADFA Course from ITRC, You can start working as Account Experts, Account Executive, Excise Executive, Taxation Executive, Account Manager, MIs Executive, Commercial Manager, Senior Faculty-Finance & Accounts, Finance Executive.</td></tr>
</table>


<!-- /END THE FEATURETTES -->
  </div>
<!-- Web programming details -->
<div id="3">

<h1> Web Programming </h1>
<ol class="breadcrumb">You are here :
  <li><a href="#courses">Courses</a></li>
  <li><a href="#3">Web Programming</a></li>
  <li class="active">ASP</li>
</ol>

<h3><code>1: Certificate in ASP.NET</code></h3>
<table class="table">
<tr><td>
Objective:</td><td>ASP.NET is a web application framework developed and marketed by Microsoft to allow programmers to build dynamic web sites, web applications and web services. ASP.NET is built on the Common Language Runtime (CLR), allowing programmers to write ASP.NET code using any supported .NET language. The ASP.NET SOAP extension framework allows ASP.NET components to process SOAP messages.</td></tr>
<tr><td>
Duration:</td><td> 2 Months</td></tr>
<tr><td>
Eligibility:</td><td> Higher Secondary or equivalent</td></tr>
<tr><td>
Curriculam:</td>
<td>
<ul><li>ASP.NET</li><li> Web Architecture</li><li> Setting up and Installin ASP.NET</li><li> overview of the ASP.NET Frame Work</li><li> Using the Standared Controls</li><li> Using the Validation Controls</li><li> Using the Rich Controls</li><li> Designing Websites with master page</li><li> Overview of Data Access</li><Li> Using SQL Data source Control</Li><li> Using List Controls</li><li> Using the Grid View Control</li><li> Using Repeater Controls</li><li> Using the Login Control</li></ul>
</td></tr>
<tr><td>Exit Profile:</td><td> After Completion of this Course from ITRC, You can start working as Entry-level Website Developer.</td></tr></table>


<ol class="breadcrumb">You are here :
  <li><a href="#courses">Courses</a></li>
  <li><a href="#3">Web Programming</a></li>
  <li class="active">PHP</li>
</ol>
<h3><code>2: Certificate in PHP</code></h3>
<table class="table">
<tr><td>Objective:</td><td> PHP is a port of the Objective-C (or Objective-J ) runtime to PHP. This adds the language features of Objective-C nestled nicely inside the syntax of Objective-C.Moka is a port of the Apple Cocoa Frameworks (or Cappuccino ). As PHP is primarily a server side scripting language the frameworks are currently non-UI ones (Foundation.)</td></tr>
<tr><td>
Duration:</td><td> 2 Months</td></tr>
<tr><td>Eligibility:</td><td> Higher Secondary or equivalent</td></tr>
 <tr><td>
Course Detail :</td><td>
<ul><li>Introduction to PHP</li> <li>Syntax of PHP</li><li> Common PHP Script Elements </li><li> Using Variables </li><li> Working With Arrays</li><li> Functions</li><li> File and Directory Handling</li><li> Including Files </li><li> File Access</li><li> Working With Forms</li><li> Processing Forms</li><li> Form Validation </li><li> Emailing Form Data</li>
<li> Addressing the Stateless nature of HTTP</li><li> Hidden Form Fields </li><li> Cookies </li><li> Sessions </li><li> Accessing Databases With PHP</li><li>Interacting With Databases</li><li>Modifying Database Records Using PHP</li><li> My SQL</li></ul>
</td></tr>
<tr><Td>
Exit Profile:</Td><td> After Completion of this Course from ITRC, Candidate will become a Developing Applications with MY SQL and PHP</td></tr>
</table>








</div>





<!-- hardware networking details go here -->
<div id="4">
<h1>Hardware and Networking</h1>
<ol class="breadcrumb">You are here :
  <li><a href="#courses">Courses</a></li>
  <li><a href="#4">Hardware and Networking</a></li>
  <li class="active">CHE</li>
</ol>

<h3><code>1: CHE (Certificate in Hardware Engineering)</code></h3>
<table class="table">
<tr><td>Objective:</td><td> The main target of the course is to prepare and enhance student's skill in Computer Maintenance, Assembly, Installations, Virus Protection, troubleshooting.</td></tr>
<tr><td>
Duration:</td><td> 3 Months</td>
</tr>
<tr><td>Eligibility:</td><td> Higher School or equivalent</td></tr>

<tr><td>Curriculam</td><td>

<ul><li>Fundamentals of DOS</li><li> Windows</li><li> Computer Hardware</li><li> Computer Assembling and Software Installation</li><li> Networking.</li></ul>
</td></tr>
<tr><Td>Exit Profile:</Td><td> After Completion of the CHE Course from ITRC, You can start working as Network Engineer, System Engineer, Cabling Designer, Hardware Executive, Technical Support Executive, Network Designer, etc.</td></tr></table>


<ol class="breadcrumb">You are here :
  <li><a href="#courses">Courses</a></li>
  <li><a href="#4">Hardware and Networking</a></li>
  <li class="active">DCHN</li>
</ol>
<h3><code>2: DCHN (Diploma in Computer Hardware and Networking)</code></h3>
<table class="table"><tr><td>
Objective:</td><td> The main target of the course is to prepare and enhance student's skill in Computer Maintenance, Assembly, Installations, Virus Protection, troubleshooting.</td></tr>
<tr><td>
Duration:</td><td> 6 Months</td></tr>
<tr><Td>
Eligibility:</Td><td> Higher Secondary or equivalent</td></tr>
<tr><td>
Curriculam</td><td>
<ul><li>
Fundamental of Computer</li><li> Computer Networking</li><li> Assembling</li><li>Hardware and Software Installation</li><li> Project.
</li>
</ul>
</td></tr>
<tr><td>
Exit Profile:</td><td> After Completion of the DCHN Course from ITRC, You can start working as Network Engineer, System Engineer, Cabling Designer, Hardware Executive, Technical Support Executive, Network Designer, etc.</td></tr></table>

<ol class="breadcrumb">You are here :
  <li><a href="#courses">Courses</a></li>
  <li><a href="#4">Hardware and Networking</a></li>
  <li class="active">ADHN</li>
</ol>
<h3><code>3: ADHN (Advance Diploma in Hardware and Networking)</code></h3>
<table class="table">
<tr><td>
Objective:</td><td> The Objective of this course is to prepare and enhance student's skill in Computer Maintenance, Assembly, Installations, Virus Protection, troubleshooting. It also prepares the participants both in Hardware, which includes the internal mechanism of all the boards, cards & peripherals used in all IBM range of computers and user level networking.
</td></tr>

<tr><td>Duration:</td><td> 1 Year</td></tr>

<tr><td>Eligibility:</td><td> Higher Secondary or equivalent</td></tr>

<tr><td>
Curriculam
</td><td>
<ul><li>
Semester I:</li>
<ul><li>
Fundamental of Computer</li><li> DOS</li><li> Windows</li><li> Software and Hardware Installation</li><li> PC Assembling and Repairing.</li></ul>
<li>

Semester II :
</li>
<ul><li>Computer Networking</li><li> Internet Networking</li> <li>Windows 2003</li> <li>Server Administration</li> <li>Project</li></ul>
</ul></td></tr>
<tr><td>Exit Profile:</td><td> After Completion of the ADHN Course from ITRC, You can start working as Network Engineer, System Engineer, Router Operator, Cabling Designer, system Administrator, IT Faculty,  Back up Operator,  Storage Specialist, Hardware Consultant, Hardware Executive,  Technical Support  Executive, Network Engineer,  Network Designer,  Asst. Manager(Tech), etc.
</td></tr></table>
</div>



<!-- Software Programming -->
<div id="5">

<h1>Software Programming</h1>
<ol class="breadcrumb">You are here :
  <li><a href="#courses">Courses</a></li>
  <li><a href="#5">Software Programming</a></li>
  <li class="active">JAVA</li>
</ol>

<h3><code>1: Certificate in JAVA (Core + Adv.)</code></h3>
<table class="table">
<tr><td>


Objective:</td><td> Understand fundamentals of programming such as variables, conditional and iterative execution, methods, etc. Understand fundamentals of object-oriented programming in Java, including defining classes, invoking methods, using class libraries, etc. Be aware of the important topics and principles of software development. Have the ability to write a computer program to solve specified problems. Be able to use the Java SDK environment to create, debug and run simple Java programs.
</td></tr>
<tr><td>Duration:</td><td> 3 Months</td></tr>
<tr><td>Eligibility:</td><td> Higher Secondary or equivalent</td></tr>
<tr><td>
Curriculam:</td><td>
<ul><li>Basics of JAVA</li><li> Applets</li> <li>JAVA Beans</li><li> Swings</li><li> Networking with JAVA using servlets</li>   <li>Cookies</li><li> RMI</li><li> JDBC</li></ul>
 </td></tr>

<tr><td>Exit Profile:</td><td> After Completion of this Course from ITRC, You can start working as java programmer
</td></tr></table>

<ol class="breadcrumb">You are here :
  <li><a href="#courses">Courses</a></li>
  <li><a href="#5">Software Programming</a></li>
  <li class="active">C , C++</li>
</ol>
<h3><code>2: Certificate Course in C, C++</code></h3>
<table class="table">
<tr><td>
Objective:</td><td>The objective of this course is to provide the student with an expertise in C++ Programming. After doing the course the student should be able to develop, design C++ with Data structure applications effectively.
</td></tr>
<tr><td>Duration:</td><td>2 Months</td></tr>

<tr><td>Eligibility:</td><td>Higher Secondary or Equivalent</td></tr>

<tr><td>Curriculam:</td><td>
<ul><li>Introduction</li><li>C/C++</li> <li>Output & Input in C/C++</li><li> Arithmetic Operations & Functions</li><li> Control Structure</li><li> Arrays & Strings</li><li> Structures & Pointers</li> <li>File Operations</li><li> Graphics using C</li><li> Introduction to OOPS Concepts</li><li> Function overloading</li><li> operator overloading</li><li> Inheritance</li></ul>
</td></tr>

<tr><td>Exit Profile:</td><td> After Completion of this Course from ITRC, You can start working as Programmer in C, C++.</td></tr>
</table>
</div>

<div id="6">
<h1>Graphics Designing</h1>

<ol class="breadcrumb">You are here :
  <li><a href="#courses">Courses</a></li>
  <li><a href="#6">Graphics Designing</a></li>
  <li class="active">DGD</li>
</ol>
<h3><code>1: DGD (Diploma in Graphic Designing)</code></h3>
<table class="table"><tr><td>
Objective:</td><td> This course covers the professional field of Graphics design, It prepares you to become skilled and innovative Graphics designers. It develops creative and practical skills, equipping you to work as an individual designer or collaboratively as a member of team. Subjects studied includes fine art, graphics, photography, 3D design, textiles, printmaking, glass, jewellery, video and digital media
</td></tr>
<tr><td>
Duration:</td><td>6 Months</td></tr>
<tr><td>
Eligibility:</td><td> High School or equivalent</td></tr>
<tr><Td>
Course Detail:</Td><td><ul><li>Windows</li><li> MS Word</li><li> PowerPoint</li><li> PageMaker</li><li> Typing Tutor</li> <li>CorelDraw</li><li> Instant Artist</li><li> Photoshop</li><li> Photo editing</li><li> Concepts for Installation of Software</li> <li>Fonts & Cliparts</li><li> Basic Knowledge of paper size</li><li> Paper Quality</li><li> Screen Printing & Offset Printing</li><li> Projects</li></ul>
</td></tr>

<tr><td>
Exit Profile:</td><td> After Completion of this Course from ITRC, You can start working as Creative Head, Graphic Designer, Logo Designer, Graphic Faculty, Layout Designer, Desktop Publisher, etc.
</td></tr></table>


<ol class="breadcrumb">You are here :
  <li><a href="#courses">Courses</a></li>
  <li><a href="#6">Graphics Designing</a></li>
  <li class="active">DTP</li>
</ol>
<h3><code>2: Certificate in D.T.P.</code></h3>
<table class="table">
<tr><td>
Objective:</td><td> To prepare students having skills to work in the field of content designing or desk top publishing were there is a great scope for them to work in printing Press, News Paper houses, Publishing companies and Advertising Industries
</td></tr>
<tr><td>Duration:</td><td> 3 Months</td></tr>

<tr><td>Eligibility:</td><td>  High School or equivalent</td></tr>
<tr><td>
Course Detail:</td><td><ul><li>Typing Tutor</li><li>PageMaker</li><li>CorelDraw</li><li>Scanning and Printing Process</li></ul></td></tr>

<tr><td>
Exit Profile:</td><td> After Completion of the DTP Course from ITRC, You can start working as Graphic Designer, Logo Designer, Desktop Publisher, Office Assistant, Data entry Operator, MIS Executive.
</td></tr></table>





</div>



  </div><!--/span-->

      <hr class="featurette-divider">

      <!-- FOOTER -->
      <footer>
      <center> <a href="index.php"><span class="glyphicon glyphicon-home"> Home</span></a> |
       <a href="about.php"><span class="glyphicon glyphicon-thumbs-up"> About </span></a> |
        <a href="courses.php"><span class="glyphicon glyphicon-folder-open"> Courses </span></a> |
<a href="gallery.php"><span class="glyphicon glyphicon-camera"> Gallery</span> |
<a href="contact.php"><span class="glyphicon glyphicon-phone-alt"> Contact</span></a>
       </center>
<!--
       <div class="spinning_icons pull-center">  
      Follow us on : <br>
        <a href="https://twitter.com/delphictechno" class="twitter" title="twitter">Twitter</a>
        <a href="https://www.facebook.com/delphictech" class="facebook" title="facebook">Facebook</a>
        <a href="http://in.linkedin.com/pub/delphic-tech/91/a7b/49/" class="linkedin" title="linkedin">Linkedin</a>
    <a href="hhttp://www.youtube.com/channel/UCDFzBhd9t1L5kIsqmuo6kcw/videos" class="youtube" title="youtube">YouTube</a>
      </div>
  </center> -->
   <p>&copy; 2014 ITEC - Kanpur , Inc.</p>
     
      </footer>
    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/google.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>
<script src="js/gallery-img.js"></script>
<script src="js/bootstrap-image-gallery.min.js"></script>
  </body>
</html>
