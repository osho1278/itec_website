
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>ITEC Kanpur</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

 <link href="css/spinningicons.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="css/carousel.css" rel="stylesheet">

<style>
#searchform {

  background:;
  
  padding:10px;
  color:black;
 margin-top:7%;
}
#myCarousel
{
margin-top:10%;

}
#taketest {

    position: absolute;
    bottom: 0;
    width: 30%;
    color:purple;
}
body { padding-top: 70px; }
</style>

  </head>
<!-- NAVBAR
================================================== -->
  <body>

  <div class="container">

        

  </div>
    <div class="navbar-wrapper">
      <div class="container">
		<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              
              <a class="navbar-brand" href="index.php" style="color:orange">ITEC- Information Technology Education Center </a>

            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="index.php">Home</a></li>
                <li><a href="about.php">About</a></li>
                <li><a href="courses.php">Courses</a></li>
                <li><a href="gallery.php">Gallery</a></li>
                <li><a href="contact.php">Contact</a></li>
                
              </ul>
            </div>

          </div>
        </div><br><br>
                      <img src="images/Graphic1.jpg" width="100%" height="180px">


      </div>
    </div>

    <!-- Carousel
    ================================================== -->
   







    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing">
    <br><br><br><br><br>
    <div class="col-sm-9">
 <div id="myCarousel" class="carousel slide " data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
        <li data-target="#myCarousel" data-slide-to="4"></li>

      </ol>
      <div class="carousel-inner">
        <div class="item active">
          <img src="pics/networks1.jpg" alt="First slide">
          <div class="container">
            <div class="carousel-caption">
              <h1 style="color:red">Computer Networking</h1>
              <p></p>
            </div>
          </div>
        </div>
        <div class="item">
          <img src="pics/website1.jpg" alt="Second slide">
          <div class="container">
            <div class="carousel-caption">
              <h1 style="color:red; font-size:48px">Web Designing</h1>
              <p></p>
            </div>
          </div>
        </div>
        <div class="item">
          <img src="pics/pl2.png" alt="Second slide">
          <div class="container">
            <div class="carousel-caption">
              <h1 style=" font-size:48px"> Software Programming</h1>
              <p></p>
            </div>
          </div>
        </div><div class="item">
          <img src="pics/tax1.jpg" alt="Second slide">
          <div class="container">
            <div class="carousel-caption">
              <h1 style="color:red; font-size:48px">Tally - Accounting / Taxation</h1>
              <p></p>
            </div>
          </div>
        </div>
        <div class="item">
          <img src="pics/microprocessor1.jpg" alt="Third slide">
          <div class="container">
            <div class="carousel-caption">
              <h1 style=" font-size:48px">Computer Hardware</h1>
              <p></p>
            </div>
          </div>
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
    </div>

</div>
<div id="searchform" class="col-sm-3" style="background-color:orange;color:black">
<center>
<h4 style="color:brown"><b>In case you have a query </b></h4>
<form role="form" method="POST" action="submit_query.php"> 
  <div class="form-group">
    <label for="exampleInputEmail1">Name</label>
    <input type="text" name="name" class="form-control" id="exampleInputEmail1" placeholder="Enter Name">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" class="form-control" name="email" id="exampleInputEmail1" placeholder="Enter email">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Contact</label>
    <input type="text" class="form-control" name="contact" id="exampleInputEmail1" placeholder="Enter Contact">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Query</label>
    <input type="text" class="form-control" name="query" id="exampleInputEmail1" placeholder="Enter Query">
  </div>
  <button type="submit" class="btn btn-success">Submit</button>
</form>
<br>
<div class="alert alert-danger" role="alert" style="background-color:orange;color:black"><h4></h4>
<a href="take_test.php" target="_blank"><button type="button" class="btn btn-danger btn-block">
Aptitude Test</button></a>



</div></div>
 <hr class="featurette-divider">

    <!-- /.carousel -->
      <div class="container">
      <div class="row">
<div class="col-lg-4">
<h2 class="featurette-heading"><span class="text-muted">Testimonials</span></h2>

</div></div>

      <div class="row">
  <div class="col-lg-4">
          <img class="img-circle" src="testimonialpics/shubham Dheer.PNG" alt="Generic placeholder image" style="width: 140px; height: 140px;">
          <h2>Shubham Dheer</h2>
          <p>"I thank ITEC for creating my interest in computers. My job of a Computer Accountant requires knowledge of computers and tally, which I do with ease and expertise because of proper guidance and support which I received from ITEC".</p>
          
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="img-circle" src="testimonialpics/Shweta Arora.PNG" alt="Generic placeholder image" style="width: 140px; height: 140px;">
          <h2>Shweta Arora</h2>
<p>
I am Er. Shweta Arora. ITEC is the best Computer institute in the field of computer training. I would like to thank ITEC for raising my technical knowledge upto the mark so that i can compete in  the professional world.

</p>
          
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="img-circle" src="testimonialpics/Swati Saxena.PNG" alt="Generic placeholder image" style="width: 140px; height: 140px;">
          <h2>Swati Saxena</h2>
          <p>I am Er. Swati Saxena and i would like to thank ITEC as it played a very important role in my success story. i have gained so much of knowledge and experience from this institute which is developing at a fast pace. I would suggest you friends to join this institute if you really want to learn computers and various other software associated because here you will get best infrastructure and quality teachers and best study material.
        
</p>
          
        </div><!-- /.col-lg-4 -->
      </div><!-- /.row -->


      <!-- START THE FEATURETTES -->


      <div class="row featurette">
        <div class="container">
        <div class="col-md-7">
          <h2 class="featurette-heading"><span class="text-muted">Director's Message</span></h2>
          <p class="lead">"The Indian IT sector is growing at a very fast pace, and the whole world is feeling the power of india’s young, intelligent and enthusiastic IT professionals". <div class="alert alert-danger" role="alert">

Very similar to the industrial revolution the Information Technology revolution is also spreading across the glob at a very rapid rate and we have once again received the chance to write our own destiny. Our future depends on our actions and reactions towards this revolution. Today, the time is demanding that we train ourselves and move towards the dawn of IT revolution.<br>
We, at ITEC are keeping with the pace of the rapidly changing career trends and our focus always stays on providing the future generation with world class education. Here, you will always find a proper mix of quality education and dedicated efforts of our faculty team.<br>
The world around us is moving quickly on the wheels of Information Technology and we hope that you too move ahead in life, get quality education and convert your dreams of a great carrier into reality.<br>
Best Wishes for a golden future.


</div>        <p>
<code>
Er. Shikhar Taneja
<br>Director & Founder
</code>
</p>

        </div>
        <div class="col-md-5">
          <br><br>          <br><br>          <br><br><img class="featurette-image img-responsive" src="galleryimages/Director's Message.jpg" alt="Generic placeholder image">
        </div>
</div>      </div>

<div class="container">
      <div class="row featurette">
        <div class="col-md-5 pull-right">
        <br> <br> <br><br> <br> <br><br> <br> <br>        <br> <br> 
          <img class="featurette-image img-responsive" src="pics/shobhit.jpg" alt="Generic placeholder image" width="200" height="200">
          <br> <br> <br>
          <img class="featurette-image img-responsive" src="pics/th.jpg" alt="Generic placeholder image" width="200" height="200">
          
        </div>
        <div class="col-md-7">
          <h4 class="featurette-heading" style="color:brown">Collaborations : <span class="text-muted"></span></h4>
          <p class="lead">Shobhit University</p>
               <div class="alert alert-success" role="alert">
     
          Shobhit University aims to create a conducive, enabling academic climate to facilitate integration of the younger generation into the logic of the present system and to develop educational means by which men and women deal critically and creatively with reality and discover how to participate in the transformation of their world.
Shobhit Institute of Engineering & Technology, Meerut has been granted Deemed-to-be University status by the Government of India, Ministry of Human Resource Development, Department of Higher Education vide its Notification No. F-9-37/2004-U.3(A) dated November 8, 2006 under section 3 of the University Grants Commission Act 1956.<br>
<a href="shobhit.php"><button class="btn btn-success">Read More</button></a>
</div>

          </p>
          <p class="lead">SHIATS University</p>

               <div class="alert alert-success" role="alert">
Sam Higginbottom Institute of Agriculture, Technology & Sciences (SHIATS), is striving to acquire a place in the arena of International Science and Technology while holding a pioneering status in India. The University offers thirty nine (39) Undergraduate Programmes, hundred and one (101) Postgraduate programmes, eighteen (18) Diploma programmes and Doctoral programe in various disciplines.
The teaching and research faculties of the University have highly qualified professionals, trained in specialized areas of Agricultural Science and Technology with several of them holding Post Graduate degrees & Doctoral Degrees from universities abroad. <br>

<a href="shiats.php"><button class="btn btn-success">Read More</button></a>
</div>


        </div>
      </div>
</div>
      <div class="row featurette">
        <div class="col-md-7">
          <h2 class="featurette-heading" style="color:brown">Affiliations<span class="text-muted"></span></h2>
          <p class="lead">
          <h3>NIELIT DOEACC</h3>
          <div class="alert alert-danger" role="alert">
          
DOEACC is a joint Scheme of the Ministry of Information Technology (Department of Electronics) and All India Council for Technical Education (AICTE), a statutory body for the development of technical education, including computer education, in the country. The Scheme is administered by the DOEACC Society, an autonomous body of the Ministry of Information Technology, Government of India. The Society is registered under the Societies Registration Act, 1860.
The objective of the Scheme is to generate quality manpower in the area of Information Technology (IT) at the national level, by utilizing the facilities and expertise available with the training institutions/organizations in the non-formal sector.


</div>
</p>
        </div><div class="col-md-1"></div>
        <div class="col-md-4"><br>          <br><br>
          <br><br>
          <br><br>

          <img class="featurette-image img-responsive" src="pics/doeacc.jpg" alt="Generic placeholder image" width="200" height="200">
          <br><br>
          <img class="featurette-image img-responsive" src="pics/neilit_logo.png" alt="Generic placeholder image" width="200" height="200">
          
        </div>
      </div>

      <hr class="featurette-divider">

      <!-- /END THE FEATURETTES -->



      <!-- FOOTER -->
      <footer>
 	    <center> <a href="index.php"><span class="glyphicon glyphicon-home"> Home</span></a> |
 	     <a href="about.php"><span class="glyphicon glyphicon-thumbs-up"> About </span></a> |
 	      <a href="courses.php"><span class="glyphicon glyphicon-folder-open"> Courses </span></a> |
<a href="gallery.php"><span class="glyphicon glyphicon-camera"> Gallery</span> |
<a href="contact.php"><span class="glyphicon glyphicon-phone-alt"> Contact</span></a>
 	     </center>
<!--
	     <div class="spinning_icons pull-center">  
    	Follow us on : <br>
        <a href="https://twitter.com/delphictechno" class="twitter" title="twitter">Twitter</a>
        <a href="https://www.facebook.com/delphictech" class="facebook" title="facebook">Facebook</a>
        <a href="http://in.linkedin.com/pub/delphic-tech/91/a7b/49/" class="linkedin" title="linkedin">Linkedin</a>
		<a href="hhttp://www.youtube.com/channel/UCDFzBhd9t1L5kIsqmuo6kcw/videos" class="youtube" title="youtube">YouTube</a>
		  </div>
  </center> -->
   <p>&copy; 2014 ITEC - Kanpur , Inc.</p>
     
      </footer>

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/google.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>
  </body>
</html>
