<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ITEC TEST</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

<?php
require_once ('include/database.php');
?>
<?php

$q = "Select * from `questions` ";
$s = $dbh-> prepare ($q);



$s-> execute();
?>
<div class="container">

<div class="col-sm-10">
<h1><kbd>ITEC Computer Education</kbd> </h1>
    <h2>Test Your Self , Choose what course is best for you !</h2>
<?php 
if(isset($_GET['mess']))

{

?>

<div class="alert alert-info" role="alert">
	
<?php
echo "You have scored ".$_GET['marks']." marks .<br>".$_GET['mess'];
?>


</div>



<?php
}

?>


<table class="table table-condensed">
<form method="POST" action="test_result.php">

<?php
$id=1;

while($e=$s->fetch(PDO::FETCH_ASSOC))
{

	echo "<tr><td><code>Question $id :".$e['question']."</code></td></tr>";
	echo "<tr><td><input type='radio' name='$id' value='".$e['option1']."'> ".$e['option1'];
	echo "<td><input type='radio' name='$id' value='".$e['option2']."'> ".$e['option2'];
	echo "<tr><td><input type='radio' name='$id' value='".$e['option3']."' > ".$e['option3'];
	echo "<td><input type='radio' name='$id' value='".$e['option4']."'> ".$e['option4'];
	$id++;
}



?>
</table>
<button type="submit" class="btn btn-success btn-block" >Submit</button>

</form>
<br><br>

</div>
</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>