
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>ITEC Kanpur</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

 <link href="css/spinningicons.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
<link rel="stylesheet" href="css/bootstrap-gallery.css">
<link rel="stylesheet" href="css/blueimp-gallery.css">
<link rel="stylesheet" href="css/bootstrap-image-gallery.min.css">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="css/carousel.css" rel="stylesheet">

<style>

#taketest {

    position: fixed;
    bottom: 0;
    width: 30%;
    color:purple;
}

</style>

  </head>
<!-- NAVBAR
================================================== -->
  <body>
    <div class="navbar-wrapper">
      <div class="container">

        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="index.php" style="color:orange">ITEC- Information Technology Education Center </a>
            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav navbar-right">
                <li><a href="index.php">Home</a></li>
                <li><a href="about.php">About</a></li>
                <li><a href="courses.php">Courses</a></li>
                <li ><a href="gallery.php">Gallery</a></li>
                <li class="active"><a href="contact.php">Contact</a></li>
                
              </ul>
            </div>
          </div>
        </div>
        <br><br>
                      <img src="images/Graphic1.jpg" width="100%" height="180px">

      </div>
    </div>

    <!-- Carousel
    ================================================== -->
   



    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->
<br><br><br><br><br><br><br><br><br><br><br>
    <div class="container marketing">

      <!-- Three columns of text below the carousel -->
     

      <!-- START THE FEATURETTES -->

<div class="col-sm-12">
<div class="container">
<table class="table">
<tr><Td colspan="2">
<h1 style="color:brown">Contact</h1>

<h3 style="color:black">  
For any queries or requests, you can contact us on following address. We would be more than happy to help you in any form possible.
<br><br></h3>
<h4 style="color:black">
</h4>
<tr><Td><h4 align="left"><small style="color:black"><i>
<span class="glyphicon glyphicon-user"> Er. Shikhar Taneja </span>
<br>
<span class="glyphicon glyphicon-phone-alt"> (+91)9984214950</span>
<br><span class="glyphicon glyphicon-envelope"> iteckanpur@gmail.com</span>
</h4>
</i>
</small>
</h4>
</Td>

<td>

<h4>
<address><small style="color:black">
<span class="glyphicon glyphicon-home"> 
  <strong>ITEC Computer Education</strong><br>
  M.I.G -18, Barra-2, Shastri Chowk,<br>
  Ratan Lal Nagar, Kanpur-27
  India<br>
  <abbr title="Phone">P:</abbr> 0512-2283550
</span>
</address>
</h4>
</td>
</table>

</div>
</div>


     
    
      <!-- /END THE FEATURETTES -->
 

      <hr class="featurette-divider">

      <!-- FOOTER -->
      <footer>
      <center> <a href="index.php"><span class="glyphicon glyphicon-home"> Home</span></a> |
       <a href="about.php"><span class="glyphicon glyphicon-thumbs-up"> About </span></a> |
        <a href="courses.php"><span class="glyphicon glyphicon-folder-open"> Courses </span></a> |
<a href="gallery.php"><span class="glyphicon glyphicon-camera"> Gallery</span> |
<a href="contact.php"><span class="glyphicon glyphicon-phone-alt"> Contact</span></a>
       </center>
<!--
       <div class="spinning_icons pull-center">  
      Follow us on : <br>
        <a href="https://twitter.com/delphictechno" class="twitter" title="twitter">Twitter</a>
        <a href="https://www.facebook.com/delphictech" class="facebook" title="facebook">Facebook</a>
        <a href="http://in.linkedin.com/pub/delphic-tech/91/a7b/49/" class="linkedin" title="linkedin">Linkedin</a>
    <a href="hhttp://www.youtube.com/channel/UCDFzBhd9t1L5kIsqmuo6kcw/videos" class="youtube" title="youtube">YouTube</a>
      </div>
  </center> -->
   <p>&copy; 2014 ITEC - Kanpur , Inc.</p>
     
      </footer>
    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/google.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>
<script src="js/gallery-img.js"></script>
<script src="js/bootstrap-image-gallery.min.js"></script>

  </body>
</html>
