
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>ITEC Kanpur</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="css/carousel.css" rel="stylesheet">

<style>

#taketest {

    position: fixed;
    bottom: 0;
    width: 30%;
    color:purple;
}

</style>

  </head>
<!-- NAVBAR
================================================== -->
  <body>
    <div class="navbar-wrapper">
      <div class="container">

        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="index.php" style="color:orange">ITEC- Information Technology Education Center </a>
            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav navbar-right">
                <li><a href="index.php">Home</a></li>
                <li class="active"><a href="about.php">About</a></li>
                <li><a href="courses.php">Courses</a></li>
                <li><a href="gallery.php">Gallery</a></li>
                <li><a href="contact.php">Contact</a></li>
                
              </ul>
            </div>
          </div>
        </div>

<br><br>
                      <img src="images/Graphic1.jpg" width="100%" height="180px">

      </div>
    </div>
    <!-- Carousel
    ================================================== -->
   
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing">

    <h1 style="color:brown">SHIATS UNIVERSITY</h1>  <!-- Three columns of text below the carousel -->
     
<p>
Sam Higginbottom Institute of Agriculture, Technology & Sciences (SHIATS), is striving to acquire a place in the arena of International Science and Technology while holding a pioneering status in India. The University offers thirty nine (39) Undergraduate Programmes, hundred and one (101) Postgraduate programmes, eighteen (18) Diploma programmes and Doctoral programe in various disciplines.
</p><p>
The teaching and research faculties of the University have highly qualified professionals, trained in specialized areas of Agricultural Science and Technology with several of them holding Post Graduate degrees & Doctoral Degrees from universities abroad. </p>
<p>The State Govt. of U .P. had certified that the Institue is a Minority Educational Institution within the meaning of clause (i) of article 30 of the Constitution of India, vide their letter No. 4894 15-80 (ii) dated the 12th of Sept. 1980. Ministry of Human Resource Development (MHRD), Government of India, New Delhi vide D.O.No.F.20-71/2005-U.3 dated 21st December 2005 also recognized the Institute as a Minority Institution.
</p>
<p>Being a Deemed-to-be University, SHIATS does not require any approval from All India Council for Technical Education (AICTE) for technical and Management courses and is empowered to award degrees as specified and notified under section 22 of University Grant Commission (UGC) Act 1956 (Notification No F.2-1/2006 U.3(A) dated 7th April 2006) issued by Ministry of Human Resource Development (MHRD), Department of Secondary & Higher Education, Govt. of India, New Delhi.</p>
<br>
<h2>Preamble:</h2>
<p>
The Sam Higginbottom Institute of Agriculture, Technology & Sciences is a united endeavor of the Christian community in India for promoting rural life and development in conformity with the Christian vision of human kind and the creation. The Institute is held in trust as a common ecumenical heritage by the Christian Churches and Christian Organizations of the country. It seeks to be a national centre of professional excellence in education and service to the people with the participation of students and faculty members from all over India. The University upholds and strives to achieve the following:
</p>
<br>a) Responsible stewardship of the environment and its resources,
<br>b) Sustainable development,
<br>c) Linkage of learning and Research to the needs and life of the people,
<br>d) Justice to the minorities, and other weaker sections of the Society, especially to women and the rural poor,
<br>e) Holistic formation of the human person in, with and through the community for leadership instilled by Christian values,
<br>f) National Unity and communal harmony,
<br>g) International fellowship and cooperation in the educational and developmental ministry in the service of the LORD JESUS CHRIST.
<p><br>
In all of the above, the University helps the young and old without coercion or compulsion to deepen their commitment to a life of service as exemplified in JESUS CHRIST; by means of presentation of the Gospel through teaching, worship and witness in accordance with the Christian belief.</p>
<p>
Students enrolled in the academic programmes are exposed to various technical training, which enables them to develop skills in their respective field. The University keeps the students informed about potential job opportunities and helps them in making prudent decisions for their future careers. It maintains and promotes close linkages and active contact with potential employers both National and Multinational, while involving their executives and facilitates their interaction with the students through lectures, discussions and classroom participation. The University also strives to prepare it’s students to take their places as farm managers, agricultural scientists, agricultural officers, extension workers, managers, educationists, agricultural administrators, agricultural scientists, bio-technologists, micro-biologists, bio-chemists, engineers, software professionals, dairy technologists, nutritionists, textile designers, theologians and pharmacists.</p>
<p>
SHIATS is a recognized member of International Association of Universities, Association of Indian Universities and Indian Agricultural Universities Association.
</p>
      <!-- FOOTER -->

      <hr class="featurette-divider">


            <footer>

      <center> <a href="index.php"><span class="glyphicon glyphicon-home"> Home</span></a> |
       <a href="about.php"><span class="glyphicon glyphicon-thumbs-up"> About </span></a> |
        <a href="courses.php"><span class="glyphicon glyphicon-folder-open"> Courses </span></a> |
<a href="gallery.php"><span class="glyphicon glyphicon-camera"> Gallery</span> |
<a href="contact.php"><span class="glyphicon glyphicon-phone-alt"> Contact</span></a>
       </center>
<!--
       <div class="spinning_icons pull-center">  
      Follow us on : <br>
        <a href="https://twitter.com/delphictechno" class="twitter" title="twitter">Twitter</a>
        <a href="https://www.facebook.com/delphictech" class="facebook" title="facebook">Facebook</a>
        <a href="http://in.linkedin.com/pub/delphic-tech/91/a7b/49/" class="linkedin" title="linkedin">Linkedin</a>
    <a href="hhttp://www.youtube.com/channel/UCDFzBhd9t1L5kIsqmuo6kcw/videos" class="youtube" title="youtube">YouTube</a>
      </div>
  </center> -->
   <p>&copy; 2014 ITEC - Kanpur , Inc.</p>
     
      </footer>

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/google.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>
  </body>
</html>
