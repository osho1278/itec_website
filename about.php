
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>ITEC Kanpur</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="css/carousel.css" rel="stylesheet">

<style>

#taketest {

    position: fixed;
    bottom: 0;
    width: 30%;
    color:purple;
}

</style>

  </head>
<!-- NAVBAR
================================================== -->
  <body>
    <div class="navbar-wrapper">
      <div class="container">

        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="index.php" style="color:orange">ITEC- Information Technology Education Center </a>
            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav navbar-right">
                <li><a href="index.php">Home</a></li>
                <li class="active"><a href="about.php">About</a></li>
                <li><a href="courses.php">Courses</a></li>
                <li><a href="gallery.php">Gallery</a></li>
                <li><a href="contact.php">Contact</a></li>
                
              </ul>
            </div>
          </div>
        </div>
<br><br>
                      <img src="images/Graphic1.jpg" width="100%" height="180px">

      </div>
    </div>
    <!-- Carousel
    ================================================== -->
   
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing">

    <h1 style="color:brown">About</h1>  <!-- Three columns of text below the carousel -->
     <b> ITEC </b> is a registered institution working in the field of providing vocational training related to <code>Computer software & hardware</code>,<code> Graphic Designing</code>,<code> Web Designing</code>, <code> Web development</code>, <code>Computer Applications</code>, <code>Finance and Accounting</code>,<code> Office Automation</code>,<code> Multimedia and Animation</code>,<code> Computer Teacher’s Training</code>,<code> Employability</code>,<code> Soft skills</code>, <code>Business management</code> and <code>Entrepreneurship Development</code>. ITEC is registered under Society Act, 1980(Registration No. K-49191). <br>
<br>
ITEC – Information technology education center, was established with a sole motive of providing computer literacy to each and every sector of society  with the moto <code>“COMPUTER LITERACY TO ALL”</code>. The aim is to impart quality job oriented technical and vocational education and training. ITEC is headed by  Er. Shikhar Taneja who has professional approach in organization’s working.
<br>
The management is completely non-political and it strives for continual growth in the management processes that ultimately lead to the success of ITEC. ITEC is standing on four pillars that may be termed as its philosophy.
<ol>

<li>DEDICATION- towards students, suppliers and academic partners.</li>
<li>QUALITY- at all levels of academics as well as processes. </li>
<li>KNOWLEDGE- updated know how of the market trends and latest technology.</li>
<li>PIONEERING VISION- to be pioneer in the field of education and training and methodologies to be used in education
</li>
</ol>

The top management of ITEC continuously strives for providing new opportunities to its students. Keeping pace with fast changing education and economic scenario as well as the market demand.<br>


<h1 style="color:brown;">Why ITEC?</h1>
According to a NASSCOM* report, the Indian IT- BPO industry is expected to grow at a rate of 12-15% this year. The industry is predicted to cross revenues of USD 100 billion during 2013-14, thus creating a demand for 28 lakh qualified IT professionals.
ITEC Computer Education prepares students to be a part of this growing industry
<br>
IT is one of the fastest growing sectors in the Indian industry, contributing nearly 7.5% to India's GDP*
According to NASSCOM HR Summit 2010, IT & BPO sector are set to add 30 million new jobs by 2020.
IT jobs are well-paying and in demand around the world.IT Job roles are expected to grow as newer & advanced technologies come up
<br>There are number of career options for different people - right from data entry operation to designing Powerpoint presentations to building mobile applications.
<br>Salient Features of ITEC COMPUTER EDUCATION
<ol>
<li>Government Registered Institute</li>
<li>Job Oriented Designed Curriculum</li>
<li>Excellent Academic Staff</li>
<li>State of art infrastructure</li>
<li>latest configured computer labs</li>
<li>Audio visual classrooms</li>
<li>University Courses available</li>
<li>No registration Fees, form fees and Admission fees</li>

      <!-- START THE FEATURETTES -->
</ol>

<h1 style="color:brown">ITEC Against other institutes :      </h1>

<table class="table table-condensed">
<tr class="danger"><th>Criteria </th> <th>ITEC</th><th>Other Institutes</th>
<tr class="info"><td>Fees</td><td>Less fees</td><td>High Fees</td>
<tr class="suc"><td>Study Material</td><td>Free of cost</td><td>Charge Extra for study material</td>
<tr class="info"><td>One student on one computer<td>Yes, 1:1 student-computer ratio<td>Two students on one computer
<tr><td>Technology<td>Latest technologically configured  computer labs<td>Old technology and Old computer systems
<tr class="info"><td>Audio visual training<td>Yes, Classrooms are enabled with LCDs and projectors<td>No such aid
<tr><td>Practical Approach<td>Daily practical lab for more practical approach<td>Practical only provide 2-3 days a week
<tr class="info"><td>Practical Duration<td>No fixed limit.Students can utilize practical lab as per their convenience<td>Fixed duration for computer lab timing.
<tr><td>Internet Broadband <td>Entire campus is wi-fi connected and has high speed broadband connection<td>No internet facility
<tr class="info"><td>Faculty and staff<td>Highly qualified and trained faculty and staff<td>Untrained faculty and staff
<tr><td>English speaking and personality training<td>Yes, Free English speaking and PD course for all the students<td>No such aid
<tr class="info"><Td>Infrastructure<td>State of art infrastructure and well maintained labs and classrooms<td>No maintenance at all
<tr><td>University collaboration <td>Yes<td>No
</table>

      <!-- /END THE FEATURETTES -->


      <hr class="featurette-divider">

      <!-- FOOTER -->
         <footer>
      <center> <a href="index.php"><span class="glyphicon glyphicon-home"> Home</span></a> |
       <a href="about.php"><span class="glyphicon glyphicon-thumbs-up"> About </span></a> |
        <a href="courses.php"><span class="glyphicon glyphicon-folder-open"> Courses </span></a> |
<a href="gallery.php"><span class="glyphicon glyphicon-camera"> Gallery</span> |
<a href="contact.php"><span class="glyphicon glyphicon-phone-alt"> Contact</span></a>
       </center>
<!--
       <div class="spinning_icons pull-center">  
      Follow us on : <br>
        <a href="https://twitter.com/delphictechno" class="twitter" title="twitter">Twitter</a>
        <a href="https://www.facebook.com/delphictech" class="facebook" title="facebook">Facebook</a>
        <a href="http://in.linkedin.com/pub/delphic-tech/91/a7b/49/" class="linkedin" title="linkedin">Linkedin</a>
    <a href="hhttp://www.youtube.com/channel/UCDFzBhd9t1L5kIsqmuo6kcw/videos" class="youtube" title="youtube">YouTube</a>
      </div>
  </center> -->
   <p>&copy; 2014 ITEC - Kanpur , Inc.</p>
     
      </footer>

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/google.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>
  </body>
</html>
