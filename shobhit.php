
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>ITEC Kanpur</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="css/carousel.css" rel="stylesheet">

<style>

#taketest {

    position: fixed;
    bottom: 0;
    width: 30%;
    color:purple;
}

</style>

  </head>
<!-- NAVBAR
================================================== -->
  <body>
    <div class="navbar-wrapper">
      <div class="container">

        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="index.php" style="color:orange">ITEC- Information Technology Education Center </a>
            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav navbar-right">
                <li><a href="index.php">Home</a></li>
                <li class="active"><a href="about.php">About</a></li>
                <li><a href="courses.php">Courses</a></li>
                <li><a href="gallery.php">Gallery</a></li>
                <li><a href="contact.php">Contact</a></li>
                
              </ul>
            </div>
          </div>
        </div>

<br><br>
                      <img src="images/Graphic1.jpg" width="100%" height="180px">

      </div>
    </div>
    <!-- Carousel
    ================================================== -->
   
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing">

    <h1 style="color:brown">SHOBHIT UNIVERSITY</h1>  <!-- Three columns of text below the carousel -->
     
<p>
Shobhit University aims to create a conducive, enabling academic climate to facilitate integration of the younger generation into the logic of the present system and to develop educational means by which men and women deal critically and creatively with reality and discover how to participate in the transformation of their world.</p>
<p>
Shobhit Institute of Engineering & Technology, Meerut has been granted Deemed-to-be University status by the Government of India, Ministry of Human Resource Development, Department of Higher Education vide its Notification No. F-9-37/2004-U.3(A) dated November 8, 2006 under section 3 of the University Grants Commission Act 1956.
</p>
<p>
The historical commitment to empower the community through education goes back as early as in 1924, when Pandit Jawaharlal Nehru Ji launched the first educational institute named “Hindu Anglo Nagrik Inter College” now known as Hindu Rashtriya Inter College, at Gangoh. This humble and noble initiative was the result of the inspiration and commitment of Babu Kedarnath Ji.
As a part of this journey to ignite the minds, a renowned agriculturist and social worker from Gangoh, district Saharanpur, our inspirer, Babu Vijendra Kumar Ji envisioned a society to cater the needs of the youth of the area and empower all sections of the community of our nation through education. This dream of Babu Ji was formalized by Dr. Shobhit Kumar and Kunwar Shekhar Vijendra voluntarily and with the spirit of social service to the community along with a team of eminent professionals, technocrats, educationists, and social workers by establishing NICE Society in 1989.
</p>
<p>
To fulfill the vision of Babu Vijendra Kumar ji, to eradicate poverty and meaningfully contribute towards the social upliftment, economic growth, employment generation, empowerment & overall development of youth on sustainable basis, NICE Society, established a number of institutions.</p>
<p>
Shobhit University aims to create a conducive, enabling academic climate to facilitate integration of the younger generation into the logic of the present system and to develop educational means by which men and women deal critically and creatively with reality and discover how to participate in the transformation of their world.</p>

<p>Shobhit Institute of Engineering & Technology, Meerut has been granted Deemed-to-be University status by the Government of India, Ministry of Human Resource Development, Department of Higher Education vide its Notification No. F-9-37/2004-U.3(A) dated November 8, 2006 under section 3 of the University Grants Commission Act 1956.
Under Section 3 of the UGC Act, Deemed-to-be University status is granted by the Central Government to those educational institutions of repute, which fulfill the prescribed standards and comply with various requirements laid down by the UGC.
Shobhit University is a fully government recognized University with the right to confer degrees as per section 3 of the UGC Act 1956.</p>

<br>
Clarification on AICTE approval: As per law, Universities do not require approval of AICTE to start or conduct technical programs.
<p>
The University will distinguish itself as a diverse, socially responsible learning community of high quality scholarship and academic rigor, sustained by Indian ethics & values. The University will draw from the cultural, intellectual and economic resources of the nation to enrich and strengthen its educational programs.</p>
<p>The University is cautiously moving to have international partnerships with reputed universities and research institutes globally. This has not only placed the University on global map but also has passed through the rigorous process of mapping and matching required for mutual accreditation of the programs and associated facilities. Over a period it has become an inbuilt mechanism of monitoring, quality control and collaboration with foreign education providers.</p>
<h2>Vision</h2>
The Shobhit University will be internationally recognized as a premier Indian University with a global perspective that educates leaders who will fashion a more humane and just world.
<h2>Mission</h2>
The core mission of the University is to promote learning in Indian tradition with international outlook. The University offers undergraduate, graduate, research scholars and professional students, the knowledge and skills needed to succeed as persons and professional in niche technical areas, and the values and sensitivity necessary to be men and women for others.
<h3>MOUs for research, faculty development and student exchange</h3>
<ul><li>
University of Westminster, London, United Kingdom
<li>Deakin University, Melbourne, Australia
<li>Ternopil Ivan Pul’uj State Technical University, Ukraine
<li>University of Lyon, France
<li>University of Hamburg, Germany
<li>SRH University, Germany
<li>ECO Solutions, Germany: Technology for Waste Water Management, Solid Waste Management, Solar Energy and Solar Cooking etc.
<li>Association of International Accountants (AIA) (UK)
<li>Institute of Laser and Aesthetic Medicine (ILAMED
<li>University’s Approvals and Recognitions
</ul>
<h3>National</h3>
<p>Rated A++, Ranked 9th among Universities established after year 2000 by Financial Express. [The Express Group Publication]
Ranked 32nd among top 100 Universities, including IITs, by Education Times.</p>
<h3>International</h3>
<ul><li>
Progression agreement with University of Westminster, London, UK for M.Tech. (VLSI, Communication) programmes.
<li>Credit Transfer arrangement with University of Westminster, London, UK for B.Tech. programmes.
<li>Young faculty capacity building scholarship with University of Westminster, London, UK.
<li>Credit Transfer arrangement with Deakin University, Australia for MBA programme.
<li>An ISO 9001:2008 certified University.
    </ul>  <!-- FOOTER -->

      <hr class="featurette-divider">

    
            <footer>
      <center> <a href="index.php"><span class="glyphicon glyphicon-home"> Home</span></a> |
       <a href="about.php"><span class="glyphicon glyphicon-thumbs-up"> About </span></a> |
        <a href="courses.php"><span class="glyphicon glyphicon-folder-open"> Courses </span></a> |
<a href="gallery.php"><span class="glyphicon glyphicon-camera"> Gallery</span> |
<a href="contact.php"><span class="glyphicon glyphicon-phone-alt"> Contact</span></a>
       </center>
<!--
       <div class="spinning_icons pull-center">  
      Follow us on : <br>
        <a href="https://twitter.com/delphictechno" class="twitter" title="twitter">Twitter</a>
        <a href="https://www.facebook.com/delphictech" class="facebook" title="facebook">Facebook</a>
        <a href="http://in.linkedin.com/pub/delphic-tech/91/a7b/49/" class="linkedin" title="linkedin">Linkedin</a>
    <a href="hhttp://www.youtube.com/channel/UCDFzBhd9t1L5kIsqmuo6kcw/videos" class="youtube" title="youtube">YouTube</a>
      </div>
  </center> -->
   <p>&copy; 2014 ITEC - Kanpur , Inc.</p>
     
      </footer>

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/google.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>
  </body>
</html>
